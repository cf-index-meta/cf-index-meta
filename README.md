
# **Active development of this repository has now moved over to [clix-meta](https://github.com/clix-meta/clix-meta) at github.**

# **Please head over there to see the development since about late 2020. This repo will be removed later this year.**


# Acknowledgement

This work is supported by the European project [IS-ENES3](https://is.enes.org/)
and by [SMHI Rossby Centre](https://www.smhi.se/en/research/research-departments/climate-research-rossby-centre2-552).


# License

**CF-index-meta** (c) 2020 by *Lars Bärring* and *Klaus Zimmermann*, Rossby
Centre, Swedish Meteorological and Hydrological Institute (SMHI).

![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) The spreadsheet and all
the metadata therein is licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
